//
//  GeneralRoute.swift
//  UalaTest
//
//  Created by Juan  Martinez on 17/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.

import Foundation
import UIKit

enum GeneralRoute: IRouter {

    case main
    case detail(parameters:[String:Any])
}

extension GeneralRoute {
    var module: UIViewController? {
        
         switch self {
         case .main:
            return MainConfiguration.setup()
         case.detail(let parameters):
             return DetailConfiguration.setup(parameters: parameters)
         }
        return nil
    }
}
