//
//  Constants.swift
//  UalaTest
//
//  Created by Juan  Martinez on 17/03/21.
//

import UIKit

let baseUrl = "https://www.themealdb.com/api/json/v1/1/"

struct Endpoint {
    
    static let searchRecipe = "search.php?s="
    static let detailRecipe = "lookup.php?i="
    static let randomRecipe = "random.php"
    
}
