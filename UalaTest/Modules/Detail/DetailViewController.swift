//
//  DetailViewController.swift
//  UalaTest
//
//  Created by Juan  Martinez on 17/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.

import UIKit

protocol IDetailViewController: class {
	var router: IDetailRouter? { get set }
}

class DetailViewController: UIViewController {
	var interactor: IDetailInteractor?
	var router: IDetailRouter?
    @IBOutlet weak var txtInstructions: UITextView!
    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var viewTitle: UIView!
    @IBOutlet weak var collection: UICollectionView!
    var recipe : MainModel.Meal?
    var ingredients : [String]?
    
	override func viewDidLoad() {
        super.viewDidLoad()
        collection.register(UINib(nibName: "IngredientsCollectionViewCell", bundle: .main),
                                forCellWithReuseIdentifier: "IngredientsCollectionViewCell")
		setupView()
        fillView()
        
    }
    @IBAction func btnReturn(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func setupView(){
        viewTitle.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 40)
        viewTitle.frame.size.width = self.view.frame.width
    }
    
    func fillView(){
        
        ingredients = ["Lentils","Onion","Carrots","Cumin","Paprika","Mint"]
        txtTitle.text = recipe?.strMeal
        txtInstructions.text = recipe?.strInstructions
        
    }
}

extension DetailViewController: IDetailViewController {
	// do someting...
}

extension DetailViewController:  UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ingredients?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IngredientsCollectionViewCell", for: indexPath) as? IngredientsCollectionViewCell
        let ingredient = self.ingredients?[indexPath.row]
        cell?.setup(ingredient: ingredient!)
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collection.frame.size.width, height: 30)
    }
    
}

extension DetailViewController {
	// do someting...
}
