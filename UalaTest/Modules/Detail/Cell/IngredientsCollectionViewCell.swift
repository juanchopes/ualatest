//
//  IngredientsCollectionViewCell.swift
//  UalaTest
//
//  Created by Juan  Martinez on 17/03/21.
//

import UIKit

class IngredientsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var txtIngredient: UILabel!
    
    func setup(ingredient : String){
        
        txtIngredient.text = ingredient
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
