//
//  MainCollectionViewCell.swift
//  UalaTest
//
//  Created by Juan  Martinez on 17/03/21.
//

import UIKit
import Kingfisher

class MainCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgRecipe: UIImageView!
    @IBOutlet weak var txtRecipeCategory: UILabel!
    @IBOutlet weak var txtRecipeName: UILabel!
    
    var recipe : MainModel.Meal?
    var callbackClick: ((MainModel.Meal) -> ())?
    
    func setup(data: MainModel.Meal){
        self.recipe = data
        let url : String = (recipe?.strMealThumb!)!
        let urlImage : URL = URL(string: url)!
        imgRecipe.kf.setImage(with: urlImage)
        txtRecipeName.text = recipe?.strMeal!
        txtRecipeCategory.text = recipe?.strCategory!

        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.addGestureRecognizer(tap)

    }
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        callbackClick!(self.recipe!)
    }

    


}
