//
//  MainInteractor.swift
//  UalaTest
//
//  Created by Juan  Martinez on 17/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.

import UIKit

protocol IMainInteractor: class {
	var parameters: [String: Any]? { get set }
    func getRecipes()
    func getRecipesBySearch(value:String)
}

class MainInteractor: IMainInteractor {
    
    var presenter: IMainPresenter?
    var manager: IMainManager?
    var parameters: [String: Any]?

    init(presenter: IMainPresenter, manager: IMainManager) {
    	self.presenter = presenter
    	self.manager = manager
    }
    
    func getRecipes(){
        manager?.getRecipes(callback: self)
    }
    
    func getRecipesBySearch(value:String) {
        manager?.getRecipesBySearch(callback: self, value: value)
    }
}

extension MainInteractor : MainManagerCallback {
    func onSuccess(recipes: MainModel.Meals) {
        self.presenter?.loadRecipes(recipe: recipes)
    }
}
