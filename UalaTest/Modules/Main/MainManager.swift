//
//  MainManager.swift
//  UalaTest
//
//  Created by Juan  Martinez on 17/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.

import Foundation
import Alamofire

protocol IMainManager: class {
    func getRecipes(callback:MainManagerCallback)
    func getRecipesBySearch(callback:MainManagerCallback , value: String)
}
protocol MainManagerCallback : class {
    func onSuccess(recipes : MainModel.Meals)
}

class MainManager: IMainManager {
    func getRecipesBySearch(callback: MainManagerCallback, value: String) {
        if(NetworkReachabilityManager()!.isReachable){
            print("____getRecipesBySearch____")
            let url = baseUrl + Endpoint.searchRecipe + value
            AF.request(url).validate().responseJSON { (responseData) in
                
                switch responseData.result{
                    case .success:
                        guard let data = responseData.data else{return}
                        print(data)
                        do{
                            let recipes = try JSONDecoder().decode(MainModel.Meals.self, from: data)
                            callback.onSuccess(recipes: recipes)
                        } catch{
                            print(error)
                        }
                    case .failure(let error):
                        print(error)
                }
            }
        }else{
            print("No hay conexión a Internet")
        }
    }
    
    func getRecipes(callback:MainManagerCallback){
        if(NetworkReachabilityManager()!.isReachable){
            print("____getRecipes____")
            let url = baseUrl + Endpoint.searchRecipe
            AF.request(url).validate().responseJSON { (responseData) in
                
                switch responseData.result{
                    case .success:
                        guard let data = responseData.data else{return}
                        print(data)
                        do{
                            let recipes = try JSONDecoder().decode(MainModel.Meals.self, from: data)
                            callback.onSuccess(recipes: recipes)
                        } catch{
                            print(error)
                        }
                    case .failure(let error):
                        print(error)
                }
            }
        }else{
            print("No hay conexión a Internet")
        }
    }
}
