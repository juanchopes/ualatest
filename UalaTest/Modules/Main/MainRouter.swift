//
//  MainRouter.swift
//  UalaTest
//
//  Created by Juan  Martinez on 17/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.

import UIKit

protocol IMainRouter: class {
	// do someting...
}

class MainRouter: IMainRouter {	
	weak var view: MainViewController?
	
	init(view: MainViewController?) {
		self.view = view
	}
}
