//
//  MainViewController.swift
//  UalaTest
//
//  Created by Juan  Martinez on 17/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.

import UIKit

protocol IMainViewController: class {
	var router: IMainRouter? { get set }
    func setRecipes(recipe: MainModel.Meals)
}

class MainViewController: BaseViewController {
	var interactor: IMainInteractor?
	var router: IMainRouter?
    @IBOutlet weak var searchBar: UISearchBar!
    var recipes: MainModel.Meals?
    @IBOutlet weak var collectionRecipes: UICollectionView!
    
    @IBOutlet weak var viewSearch: UIView!
    

	override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        searchBar.showsCancelButton = true
        setupView()
        collectionRecipes.register(UINib(nibName: "MainCollectionViewCell", bundle: .main),
                                forCellWithReuseIdentifier: "MainCollectionViewCell")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getRecipes()
        
    }
    
    func setupView(){
        viewSearch.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 40)
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        viewSearch.frame.size.width = self.view.frame.width
    }
    
    /** Hide Keyboard*/
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func getRecipes(){
        self.showLoading()
        guard let interactor = interactor else { return}
        interactor.getRecipes()
    }
    
    func setRecipes(recipe: MainModel.Meals){
        self.recipes = recipe
        self.collectionRecipes.reloadData()
        self.removeLoading()
    }
    
    func getRecipesSearch(string: String){
        self.showLoading()
        guard let interactor = interactor else { return}
        interactor.getRecipesBySearch(value: string)
    }
    
    func goToDeatilRecipe(recipe : MainModel.Meal){
        self.navigate(type: .push, module: GeneralRoute.detail(parameters: ["recipe":recipe]))    }
}

extension MainViewController: IMainViewController {
	// do someting...
}

//MARK: - UICollectionView
extension MainViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if recipes != nil {
            return recipes?.meals?.count ?? 0
        }else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainCollectionViewCell", for: indexPath) as? MainCollectionViewCell
        let recipe = self.recipes?.meals?[indexPath.row]
        cell?.setup(data: recipe!)
        cell!.callbackClick = { product in
            self.goToDeatilRecipe(recipe: product)
        }

        return cell!
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.frame.size.width)/2 - 35, height: 250)
    }

}

extension MainViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n"{ return false}
        let textactual = searchBar.text! + text
        self.getRecipesSearch(string: textactual)
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.text = ""
        self.getRecipes()
    }
}
