//
//  MainPresenter.swift
//  UalaTest
//
//  Created by Juan  Martinez on 17/03/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.

import UIKit

protocol IMainPresenter: class {
    func loadRecipes(recipe: MainModel.Meals)
}

class MainPresenter: IMainPresenter {	
	weak var view: IMainViewController?
	
	init(view: IMainViewController?) {
		self.view = view
	}
    
    func loadRecipes(recipe: MainModel.Meals){
        self.view?.setRecipes(recipe: recipe)
    }
}
